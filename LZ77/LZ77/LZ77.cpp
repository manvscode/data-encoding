#include "LZ77.h"


namespace DataEncoding {


LZ77::LZ77( )
{
}

LZ77::~LZ77( )
{
}
   //1. Set the coding position to the beginning of the input stream;
   //2. find the longest match in the window for the lookahead buffer;
   //3. output the pair (P,C) with the following meaning:
   //       * P is the pointer to the match in the window;
   //       * C is the first character in the lookahead buffer that didn't match; 
   //4. if the lookahead buffer is not empty, move the coding position (and the window) L+1 characters forward and return to step 2. 
void LZ77::encode( const vector<byte> &inBuffer, vector<byte> &outBuffer )
{
	static byte slidingWindow[ LZ77_WINDOW_BUFFER_SIZE ];

	for( unsigned int i = 0; i < inBuffer.size( ); i++ )
	{
		const byte *pLookAheadBufferStart = &inBuffer[ i ];
		int nLongestMatchOffset = -1;
		unsigned int matchLength = 0;

		for( unsigned int j = 0; j < LZ77_LOOK_AHEAD_BUFFER_SIZE; j++ )
		{
			for( unsigned int k = 0; k < LZ77_WINDOW_BUFFER_SIZE; k++ )
			{
				if( slidingWindow[ k ] == inBuffer[ i + j ] )
				{
					if( nLongestMatchOffset == -1 )
						nLongestMatchOffset = k;
				}

			}
		}

	}

}

void LZ77::decode( const vector<byte> &inBuffer, vector<byte> &outBuffer )
{
}

} // end of namespace