#ifndef _LZ77_H_
#define _LZ77_H_

#include <vector>
#include <bitset>
using namespace std;

namespace DataEncoding {

#define LZ77_WINDOW_BUFFER_SIZE			4096
#define LZ77_LOOK_AHEAD_BUFFER_SIZE		1024

class LZ77
{
  public:
	typedef unsigned char byte;

  protected:
	byte m_SlidingWindow[ LZ77_WINDOW_BUFFER_SIZE ];
	vector<byte> m_InBuffer;
	vector<byte> m_OutBuffer;


	typedef bitset<31> PhraseToken; // bit 30 = PhraseTokenFlag,
									// bit 18 - 29 = sliding window offset, 
									// bit 8 - 17 = length, bit 0 - 7 = unmatch symbol
	typedef bitset<9> SymbolToken; // bit 8 = Symbol token flag, bit 0 - 7 = unmatched symbol
  public:
	LZ77( );
	virtual ~LZ77( );

	
	static void encode( const vector<byte> &inBuffer, vector<byte> &outBuffer );
	static void decode( const vector<byte> &inBuffer, vector<byte> &outBuffer );

};

} // end of namespace
#endif