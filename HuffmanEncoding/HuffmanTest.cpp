/*
 *	HuffmanTest.cpp
 *
 *	Huffman encoder/decoder test driver.
 *	Coded by Joseph A. Marrero
 *	http://www.L33Tprogrammer.com/
 *	December 16, 2007 - Merry Christmass!
 */
#include <windows.h>
#include <vector>
#include <bitset>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>
using namespace std;
#include "Huffman/huffman.h"

int main(int argc, char *argv[] )
{
	HANDLE hConsole;

	hConsole = GetStdHandle( STD_OUTPUT_HANDLE );

	string testFile = "test.txt";
	/*
	 *	Test 1
	 */
	Encoding::Huffman huffEncoderDecoder;
	assert( huffEncoderDecoder.encode( testFile ) );
	huffEncoderDecoder.save( testFile + ".compressed" );

	assert( huffEncoderDecoder.decode( testFile + ".compressed" ) );
	huffEncoderDecoder.save( testFile + ".decompressed" );

	/*
	 *	Test 2
	 */
	unsigned char pBuffer[] = "Hello World!";//"Call me Ishmael. Some years ago - never mind how long precisely - having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating the circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodically knocking people's hats off - then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball. With a philosophical flourish Cato throws himself upon his sword; I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.";
	vector<unsigned char> org( pBuffer, pBuffer + sizeof(pBuffer) );
	vector<unsigned char> compressed;
	assert( Encoding::Huffman::encode( org, compressed ) );


	vector<unsigned char> decompressed;

	assert( Encoding::Huffman::decode( compressed, decompressed ) );

	
	string strCompressed( (const char *) &compressed[ 0 ], compressed.size( ) );
	string strDecompressed( (const char *) &decompressed[ 0 ], strlen( (const char *) &decompressed[ 0 ] ) );
	
	SetConsoleTextAttribute( hConsole, 15 );
	cout << setw( 14 ) << setiosflags( ios::left ) << "Orginal:";
	SetConsoleTextAttribute( hConsole, 10 );
	cout << "<" << pBuffer << ">" << endl << endl << endl;
	
	SetConsoleTextAttribute( hConsole, 15 );
	cout << setw( 14 ) << setiosflags( ios::left ) << "Compressed:";
	SetConsoleTextAttribute( hConsole, 14 );
	cout << "<" << strCompressed << ">" << endl << endl << endl;
	
	SetConsoleTextAttribute( hConsole, 15 );
	cout << setw( 14 ) << setiosflags( ios::left ) << "Decompressed:";
	SetConsoleTextAttribute( hConsole, 12 );
	cout << "<" << strDecompressed << ">" << endl;


	return 0;
}